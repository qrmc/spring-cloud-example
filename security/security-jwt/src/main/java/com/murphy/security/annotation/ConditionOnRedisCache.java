package com.murphy.security.annotation;

import com.murphy.security.condition.OnRedisCacheCondition;
import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * com.rizhao.collective.common.condition.OnRedisCacheCondition
 * #matches(org.springframework.context.annotation.ConditionContext, org.springframework.core.type.AnnotatedTypeMetadata)
 * 返回true时此注解标识的bean会被注入Spring
 * @author dongsufeng
 * @date 2021/3/1 10:28
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@Conditional(OnRedisCacheCondition.class)
public @interface ConditionOnRedisCache {
}
