package com.murphy.security.exception;
/**
 *
 * @author dongsufeng
 * @date 2021/9/8 19:56
 * @version 1.0
 */
public class CacheException extends RuntimeException{
	public CacheException() {
	}

	public CacheException(String message) {
		super(message);
	}

	public CacheException(Throwable cause) {
		super(cause);
	}

	public CacheException(String message, Throwable cause) {
		super(message, cause);
	}
}
