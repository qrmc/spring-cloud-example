package com.murphy.security.cache;

import com.murphy.security.exception.CacheException;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author dongsufeng
 * @date 2021/9/8 19:57
 * @version 1.0
 */
public class MapCache<K,V> implements Cache<K,V>{

	private final Map<K, V> map;

	public MapCache(Map<K, V> backingMap) {
		if(backingMap == null) {
			throw new IllegalArgumentException("Backing map cannot be null.");
		} else {
			this.map = backingMap;
		}
	}

	@Override
	public V get(K var1) throws CacheException {
		return this.map.get(var1);
	}

	@Override
	public V put(K var1, V var2,long timeout, TimeUnit timeUnit) throws CacheException {
		return this.map.put(var1,var2);
	}

	@Override
	public V remove(K var1) throws CacheException {
		return this.map.remove(var1);
	}

	@Override
	public void clear() throws CacheException {
		this.map.clear();
	}

	@Override
	public boolean hasKey(K var1) throws CacheException {
		return this.map.containsKey(var1);
	}
}
