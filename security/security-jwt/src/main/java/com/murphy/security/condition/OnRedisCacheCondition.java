package com.murphy.security.condition;

import com.murphy.security.properties.BaseProperties;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.lang.NonNull;

/**
 * 满足条件后注入bean，matches 返回true
 * @author dongsufeng
 * @date 2021/9/8 19:30
 * @version 1.0
 */
public class OnRedisCacheCondition implements Condition {

    @Override
    public boolean matches(@NonNull ConditionContext context, @NonNull AnnotatedTypeMetadata metadata) {
        return Boolean.parseBoolean(context.getEnvironment().getProperty(BaseProperties.ENABLE_REDIS_CACHE));
    }
}
