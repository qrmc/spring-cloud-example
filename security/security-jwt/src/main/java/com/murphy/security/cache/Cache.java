package com.murphy.security.cache;

import com.murphy.security.exception.CacheException;

import java.util.concurrent.TimeUnit;

/**
 *
 * @author dongsufeng
 * @date 2021/9/8 19:55
 * @version 1.0
 */
public interface Cache<K, V> {

	V get(K var1) throws CacheException;

	V put(K var1, V var2,long timeout, TimeUnit timeUnit) throws CacheException;

	V remove(K var1) throws CacheException;

	void clear() throws CacheException;

	boolean hasKey(K var1) throws CacheException;
}
