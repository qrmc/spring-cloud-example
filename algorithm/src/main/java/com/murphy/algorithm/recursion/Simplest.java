package com.murphy.algorithm.recursion;

import java.util.HashMap;
import java.util.Map;

/**
 * 最简单的递归
 * 最终推荐人
 * @author dongsufeng
 * @date 2020/9/10 11:26
 * @version 1.0
 */
public class Simplest {
	static Map<String,String> map=new HashMap<>();
	static {
		map.put("1","3");
		map.put("3","4");
		map.put("4","2");
		map.put("2","");
	}
	/**
	 *
	 * @param n 当前的人
	 * @return
	 */
	public static String f(String n){
		String p = map.get(n);
		if (p==null || "".equals(p)){
			return n;
		}
		return f(p);
	}

	public static void main(String[] args) {
		System.out.println(Simplest.f("3"));;
	}
}
