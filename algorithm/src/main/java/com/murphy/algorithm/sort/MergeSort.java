package com.murphy.algorithm.sort;
/**
 * 归并排序
 * @author dongsufeng
 * @date 2020/9/15 17:48
 * @version 1.0
 */
public class MergeSort {
	/**
	 *
	 * @param items 数组
	 * @param s,e
	 */
	public void sort(int [] items,int s,int e){
		if (s>=e){
			return;
		}
		int i = (s+e) / 2;
		sort(items,s,i);
		sort(items,i+1,e);
		merge(items,s,i,e);
	}

	/**
	 *
	 * @param items 数组
	 * @param s 开始
	 * @param q 中间
	 * @param e 结算
	 */
	public void merge(int [] items,int s ,int q,int e){
	}
}
