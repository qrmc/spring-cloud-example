package com.murphy.algorithm.queue;
/**
 * 基于链表实现队列
 * @author dongsufeng
 * @date 2020/9/8 17:13
 * @version 1.0
 */
public class LinkedQueue {

	private static class Node<E>{
		private E e;
		private Node<E> next;

		Node(E e,Node<E> next){
			this.e=e;
			this.next=next;
		}
	}

	/**
	 * 最大容量
	 */
	private int count;
	/**
	 * 当前个数
	 */
	private int size;
	private Node<String> head;
	private Node<String> last;

	LinkedQueue(int count){
		this.count=count;
		size=0;
	}

	/**
	 * 进队列
	 * @param item
	 * @return
	 */
	public boolean enQueue(String item){
		if (size == count){
			return false;
		}
		Node<String> newNode = new Node<>(item,null);
		if (last== null){
			head = newNode;
		}else {
			last.next = newNode;
		}
		last = newNode;
		++size;
		return true;
	}

	public String deQueue(){
		if (size==0){
			return null;
		}
		String e = head.e;
		head=head.next;
		--size;
		return e;
	}

	public static void main(String[] args) {
		LinkedQueue linkedQueue = new LinkedQueue(5);
		linkedQueue.enQueue("11");
		linkedQueue.enQueue("22");
		linkedQueue.enQueue("33");
		linkedQueue.enQueue("44");
		linkedQueue.enQueue("55");
		for (int i=0;i<3;i++){
			System.out.println(linkedQueue.deQueue());
		}
		linkedQueue.enQueue("66");
		System.out.println(linkedQueue.size);
	}
}
