package com.murphy.gatewaymain.request;

import lombok.Data;

/**
 *
 * @author dongsufeng
 * @date 2021/11/9 13:51
 * @version 1.0
 */
@Data
public class OrderRequest extends BaseRequest{

	private String loanNo;

	private String desc;
}
