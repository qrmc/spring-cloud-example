package com.murphy.gatewaymain.function;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.murphy.gatewaymain.request.BaseRequest;
import com.murphy.gatewaymain.request.XdRequest;
import com.murphy.gatewaymain.utils.SignUtil;
import org.reactivestreams.Publisher;
import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/**
 * 资方加密
 * @author dongsufeng
 * @date 2021/11/9 10:50
 * @version 1.0
 */
@Component
public class GongShangEncryptFunction implements RewriteFunction<BaseRequest,String> {

	public static final String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANe2bmG2t11hweL4rd48JsBdeyZfRy8phAUqhs8sBi8lHFn8L3+VDkZH35BRPZPzDJ30a2lehBZjL+FXZCWIub16baGQgFjT2Jiod3xCa0uIvFDrpS28NAMb+gcw1VAVFJL1FVHtNKk2GYLQjZUR+oX884mKbtG4e49P9lMH7Z1vAgMBAAECgYALJEAVSfO0ngz+pSuN0/uIagunUrqBhBpujeDCqJp1KuyI9U6av18qYCH6+Uc98grPycUWfyxBX8QkVng0vBgjyfzeboPUbh0MFx5DA5lJ2yMv+oPLXIEjTcA4sVUxoRXLrSSETTGIuigFuNctc4vBx755hxGn1VnBhuEYsvu6MQJBAO51Br/xw3m5trlJN0ko4P7nlziFWhB0wopCGv6tLeQYvkcw98jz+EplARNcTP/0aQrggHM5UPP7cW7j6kp+AQkCQQDnlQwAG2gOJ30cJxMlQH23NE9ju3poUzHUiJ8qOXSHZOVScYP8VWtKfFMWSiQXriIgQ34LsyDLo/k6MJ3TG+C3AkBGfwR61I+0uenCR1n34AT8dx0m0Y251br5wudWKX6qs4H1bA2lNDNQUyIJRj1hYjF3zL1M00ISj2COpwTJ9wx5AkB9Q1CnaiuhpFh29ufTOYwGocPjhVATyBRnCrNVSpiud7PXIVGsFqQfORpULyxQpr8MxpUSTQULQZmYkR19SFIHAkEA4WW+WwuP7PGbwAyjrlcJo6e24zQ0N189zCfqULWnKGTYBm5LhMyJGEmVzjsPb148PD4Z7yGdhrWQtPlfWbCfTQ==";
	public static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXtm5htrddYcHi+K3ePCbAXXsmX0cvKYQFKobPLAYvJRxZ/C9/lQ5GR9+QUT2T8wyd9GtpXoQWYy/hV2QliLm9em2hkIBY09iYqHd8QmtLiLxQ66UtvDQDG/oHMNVQFRSS9RVR7TSpNhmC0I2VEfqF/POJim7RuHuPT/ZTB+2dbwIDAQAB";
	@Override
	public Publisher<String> apply(ServerWebExchange exchange, BaseRequest baseRequest) {
		System.out.println("==========="+ JSON.toJSONString(baseRequest));
		if (exchange.getRequest().getMethodValue().equals(HttpMethod.GET.name())){
			return Mono.just(JSON.toJSONString(baseRequest));
		}
		try {
			String key = SignUtil.getRandom();
			String content = JSON.toJSONString(baseRequest);
			String reqData = SignUtil.encrypt4Base64(content, key);
			String randomKey = SignUtil.encryptByPublicKey4Pkcs5(key.getBytes(StandardCharsets.UTF_8), publicKey);
			XdRequest xdRequest = new XdRequest();
			xdRequest.setChannelId("100000");
			xdRequest.setTime(String.valueOf(System.currentTimeMillis()));
			xdRequest.setVersion("1.0");
			xdRequest.setData(reqData);
			xdRequest.setRandomKey(randomKey);
			String signData = SignUtil.sign(JSON.toJSONString(xdRequest, SerializerFeature.MapSortField)
					.getBytes(StandardCharsets.UTF_8), privateKey);
			xdRequest.setSign(signData);
			return Mono.just(JSON.toJSONString(xdRequest));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Mono.empty();
	}
}
